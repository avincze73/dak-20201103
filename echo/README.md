# Echo project

## Deploying the first pod to aks cluster
```bash
mkdir git
cd git
git clone 

docker login

docker-compose -f echo/docker-compose.yaml build
docker-compose -f echo/docker-compose.yaml push
kubectl apply -f echo/01-deployment.yaml
kubectl get pod

kubectl logs echo -f

kubectl delete pod echo
# or
kubectl delete -f echo/01-deployment.yaml
```

[Főoldal](../README.md)
