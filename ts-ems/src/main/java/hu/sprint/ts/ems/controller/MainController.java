package hu.sprint.ts.ems.controller;

import hu.sprint.ts.ems.config.TsProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@Slf4j
public class MainController {

    @Autowired
    private TsProperties tsProperties;


    @GetMapping({"/main"})
    public String mainGet(Model model) {
        model.addAttribute("pageTitle", "Főoldal");
        model.addAttribute("nnVersion", tsProperties.getVersion());
        return "main";
    }


    @PostMapping({"/main"})
    public String mainPost(Model model) {
        model.addAttribute("pageTitle", "Főoldal");
        model.addAttribute("nnVersion", tsProperties.getVersion());
        return "main";
    }


    @GetMapping({"/login", "/"})
    public String login(Model model) {
        model.addAttribute("pageTitle", "Főoldal");
        return "login";
    }



}
