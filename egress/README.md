# Azure Egress

## Setup Azure cli
```bash
az aks show --resource-group sprint --name sprint-cluster --query nodeResourceGroup -o tsv
# MC_sprint_sprint-cluster_westeurope

az network public-ip create \
    --resource-group MC_sprint_sprint-cluster_westeurope \
    --name myAKSPublicIP \
    --allocation-method static

az role assignment create \
    --assignee b2549e81-978a-4fba-be94-94b6e8655fc8 \
    --role "Network Contributor" \
    --scope /subscriptions/582dcfb2-6d23-47b9-8a2d-ca8875504901/resourceGroups/MC_sprint_sprint-cluster_westeurope

az network public-ip create \
    --resource-group sprint \
    --name myAKSPublicIP \
    --allocation-method static


az network public-ip list --resource-group MC_sprint_sprint-cluster_westeurope --query publicIp.ipAddress --output tsv

#52.174.176.235

kubectl apply -f egress/egress-service.yaml

kubectl run -it --rm aks-ip --image=ubuntu
apt update && apt install curl -y
curl -s checkip.dyndns.org



az aks show -g sprint -n sprint-cluster --query "identity"
az identity create --name myIdentity --resource-group sprint
```

[Főoldal](../README.md)
