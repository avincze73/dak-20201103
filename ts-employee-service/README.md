# TS-Employee-Service

## ts-employee-service
- This project can integrate strings from configmap into application.properties and kubernetes integration
```bash
mvn clean package
docker-compose -f ts-employee-service/docker-compose.yaml build
docker-compose -f ts-employee-service/docker-compose.yaml push
kubectl apply -f ts-employee-service/k8s/configmap.yaml
kubectl apply -f ts-employee-service/k8s/rbac.yaml
kubectl apply -f ts-employee-service/k8s/deployment-and-service.yaml

curl 20.50.229.155:9002/actuator/info
curl 20.50.229.155:9002/actuator/health
curl 20.50.229.155:9002/actuator/info
```

## ts-employee-service with helm
- This project can integrate strings from configmap into application.properties and kubernetes integration
```bash
helm install ts-employee-service/helm/ts-employee-service --generate-name --debug --dry-run

helm install ts-employee-service ts-employee-service/helm/ts-employee-service 


helm ls
helm del ts-employee-service-1603697422
helm history ts-employee-service-1603698631

helm upgrade ts-employee-service-1603698631 ts-employee-service/helm/ts-employee-service

helm rollback  ts-employee-service-1603698631 1


helm install ts-employee-service/helm/ts-employee-service --generate-name
helm uninstall ts-employee-service-1603697941



```





[Főoldal](../README.md)