# Azure

## Setup Azure cli
```bash
sudo curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
az login 
az aks get-credentials --resource-group KubernetesTraining2020 --name tsm-cluster
kubectl config view
kubectl version
kubectl get nodes
```


## Deploying the first application to azure aks
```bash
kubectl create deployment ts-greeting --image=avincze73/ts-greeting
kubectl get deployment
kubectl expose deployment ts-greeting --type=LoadBalancer --port=8001
kubectl get svc

# testing the application
curl 20.54.147.74:8001/greetings

kubectl delete deploy ts-greeting
kubectl delete svc ts-greeting
```

[Főoldal](../README.md)
