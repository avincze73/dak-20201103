# DAK-TSM

> A project contains the training material of the course.

## Table of contents

- [Post installation tasks of Ubuntu 20.04](ubuntu/README.md)
- [Setup Azure CLI](azure/README.md)
- [Deploying the echo pod the AKS cluster](echo/README.md)

- [Microservice architecture](diagram/README.md)

- [Kubernetes in depth](ts-greeting/README.md)
- [Spring Boot Web Application](ts-ems/README.md)

- [Ingress](ingress/README.md)
- [Istio](istio/README.md)

- [Further samples](k8s/README.md)

- [Kubernetes administration](administration/README.md)
- [Kubernetes with gitlab](gitlab/README.md)

- [Spring boot cloud I.](ts-employee-service/README.md)
- [Spring boot cloud II.](ts-main-service/README.md)

- [Microservice I.](ts-currency-exchange/README.md)
- [Microservice II.](ts-currency-conversion/README.md)


Dive in to [TS-Greeting](ts-greeting/README.md).
