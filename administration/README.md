# Kubernetes Administration

## Master node services
```bash

```


## Resource quotas
- Kubernetes resources can be managed
- We want to restrict the cluster resource usage (cpu)
- Resource quotas can be attached to namespaces
- __ResourceQuota__ (200m cpu ) and __ObjectQuota__ objects
- __Request capacity__ is an explicit request for resources. The minimum amount of resources the pod needs
- __Resource limit__. A container can not handle more resources than specified.

Resource limits in a namespace

|Resource|Description|
|--------|-----------|
|requests.cpu|Sum of cpu requests of all pods can not exceed this value|
|requests.mem|Sum of mem requests of all pods can not exceed this value|
|requests.storage|Sum of storage requests of all pvc can not exceed this value|
|limits.cpu|Sum of cpu limits of all pods can not exceed this value|
|limits.memory|Sum of mem limits of all pods can not exceed this value|

Object limits in a namespace

|Resource|Description|
|--------|-----------|
|pods|Total number of pods in a namespace|
|pods|Total number of services in a namespace|
|...|Total number of ... in a namespace|


## Namespaces

- Virtual clusters within the physical cluster
- Logically separates cluster resources
- __default__ is the default namespace
- __kube-system__ namespace for system resources
- Useful in case of multiple projects or teams
- Resource names must be unique within a namespace
- Resources can be limited on a per namespace basis


```bash
kubectl create namespace tsm
kubectl get ns
kubectl apply -f administration/01-resourcequota.yaml
kubectl apply -f administration/02-no-quotas.yaml
kubectl get deploy --namespace=sprint-ns

# limits must be specified
kubectl describe rs ts-greeting-7fbff64bd4 --namespace=sprint-ns
kubectl delete -f administration/02-no-quotas.yaml

kubectl apply -f administration/03-with-quotas.yaml
kubectl get deploy --namespace=sprint-ns
# only two pods are running
# where is the third one?
kubectl describe rs ts-greeting --namespace=sprint-ns
kubectl get quota --namespace=sprint-ns
kubectl describe quota  compute-quota --namespace=sprint-ns

kubectl apply -f administration/04-defaults.yaml
kubectl describe limits  limits --namespace=sprint-ns

kubectl delete -f administration/01-resourcequota.yaml
```

## User management
- A __normal user__ can access the cluster through kubectl
- A __service user__ is managed by a kubernetes object



[Főoldal](../README.md)
