# TS-Main-Service

## ts-main-service
```bash
mvn clean package
docker-compose -f ts-main-service/docker-compose.yaml build
docker-compose -f ts-main-service/docker-compose.yaml push
kubectl apply -f ts-main-service/k8s/deployment-and-service.yaml
# show the error in logs
kubectl logs ts-main-service-5b4c9fccd9-k49ff -f
kubectl apply  -f ts-main-service/k8s/rbac.yml

kubectl create configmap ts-main-service --from-literal=TRAINING=dak --from-literal=FIRM=sprint
kubectl describe configmap ts-main-service
kubectl delete -f ts-main-service/k8s/deployment-and-service.yaml
kubectl apply -f ts-main-service/k8s/deployment-and-service.yaml

kubectl scale deployment ts-main-service --replicas=0
kubectl scale deployment ts-main-service --replicas=1

```


[Főoldal](../README.md)
