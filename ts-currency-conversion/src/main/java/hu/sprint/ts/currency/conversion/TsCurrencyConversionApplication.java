package hu.sprint.ts.currency.conversion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"hu.sprint.ts.currency.conversion"})
@EnableFeignClients
public class TsCurrencyConversionApplication {

	public static void main(String[] args) {
		SpringApplication.run(TsCurrencyConversionApplication.class, args);
	}

}
