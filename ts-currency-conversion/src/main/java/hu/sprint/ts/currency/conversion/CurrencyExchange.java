package hu.sprint.ts.currency.conversion;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CurrencyExchange {

    private Long id;

    private String fromCurrency;

    private String toCurrency;

    private BigDecimal exchangeRate;

    private String environmentInfo;

    public CurrencyExchange(String fromCurrency, String toCurrency, BigDecimal exchangeRate) {
        this.fromCurrency = fromCurrency;
        this.toCurrency = toCurrency;
        this.exchangeRate = exchangeRate;
    }
}
